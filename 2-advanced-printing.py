#String Concatenation
print "Spam " + "and " + "eggs"
#output: "Spam and eggs"




#Explicit String Conversion
print "The value of pi is around " + str(3.14)
#output: "The value of pi is around 3.14"




#String Formatting with %, Part 1
string_1 = "Camelot"
string_2 = "place"
print "Let's not go to %s. It's a silly %s." % (string_1, string_2)
#output:  Let's not go to Camelot. It's a silly place"




#String Formatting with %, Part 2
name = raw_input("What is your name?")
quest = raw_input("What is your quest?")
color = raw_input("What is your favorite color?")

print "Ah, so your name is %s, your quest is %s, and your favorite color is %s." % (name, quest, color)
#output: Ah, so your name is 'batman', your quest is 'blabla', and your favorite color is 'blabla'."