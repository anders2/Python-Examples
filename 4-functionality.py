isOdd = lambda x: x % 2
increase = lambda x: x + 1
sum2Number = lambda x, y: x + y

print "\nisOdd = lambda x: x % 2"
print "increase = lambda x: x + 1"
print "sum2Number = lambda x, y: x + y\n"

numbers = ["5", "1132", "455", "12673"]
print "numbers = ", numbers, "\n"

numbers = map(int, numbers)
print "map(int, numbers) = ", numbers, "\n"

numbersIncrease = map(increase, numbers)
print "map(increase, numbers) = ", numbersIncrease

numbersIsOdd = map(isOdd, numbers)
print "map(isOdd, numbers) = ", numbersIsOdd

numbersOdds = filter(isOdd, numbers)
print "filter(isOdd, numbers) = ", numbersOdds

sumOfNumbers = reduce(sum2Number, numbers)
print "reduce(sum2Number, numbers)", sumOfNumbers