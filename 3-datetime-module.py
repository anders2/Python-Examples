# import the datetime object from the datetime module:
from datetime import datetime

# get the datetime from object to now variable.
now = datetime.now()

# Date with dd/mm/yyyy
print '%s/%s/%s' % (now.day, now.month, now.year)

# Date with time: hh:mm/ss
print '%s:%s:%s' % (now.hour, now.minute, now.second)

# Date with complate
print '%s/%s/%s %s:%s:%s' % (now.day, now.month, now.year, now.hour, now.minute, now.second)

# Date format with `strftime` or `format` methods,
# Have a look at the doc for other options,
# https://docs.python.org/2/library/datetime.html#strftime-and-strptime-behavior
print now.strftime("%Y-%m-%d %H:%M")
print "{:%Y-%m-%d %H:%M}".format(now)
