Python-Example
==============

This repo contains small algorithms and simple python module examples. This will be constantly updated.


Table of Contents:

  1. String methods.
  2. Advanced Printing
  3. Datetime module.
  4. Functionality
