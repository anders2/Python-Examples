# String methods let you perform specific tasks for strings.
# We'll focus on four string methods:
#  1. len()   -> turns number of letters in string. 
#  2. lower() -> makes a string completely lower case. 
#  3. upper() -> makes a string completely upper case.
#  4. str()   -> turns non-strings into strings.


parrot = "Norwegian Blue"
pi = 3.14

print len(parrot)       # output: 14
print parrot.lower()    # output: "norwegian blue"
print parrot.upper()    # output: "NORWEGIAN BLUE"
print str(pi)           # output: "3.14"
